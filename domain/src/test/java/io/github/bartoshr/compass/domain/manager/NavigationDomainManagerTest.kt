package io.github.bartoshr.compass.domain.manager

import android.location.Location
import io.github.bartoshr.compass.domain.BaseDomainUnitTest
import io.github.bartoshr.compass.domain.model.location.Coordinates
import io.github.bartoshr.compass.persistence.manager.LocationPersistenceManager
import io.github.bartoshr.compass.utility.compass.CompassManager
import io.github.bartoshr.compass.utility.location.LocationManager
import io.reactivex.Flowable
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner.StrictStubs

@RunWith(value = StrictStubs::class)
class NavigationDomainManagerTest : BaseDomainUnitTest() {

  //region Test Data

  internal companion object TestData {
    const val TEST_LATITUDE = "54.377300"
    const val TEST_LONGITUDE = "18.616600"
  }

  //endregion

  //region Constructor arguments mocks

  @Mock
  private lateinit var compassManager: CompassManager

  @Mock
  private lateinit var locationManager: LocationManager

  @Mock
  private lateinit var persistence: LocationPersistenceManager

  //endregion

  //region Test Object

  private lateinit var navigationManager: NavigationDomainManager

  //endregion

  //region Setup

  @Before
  fun setUp() {
    navigationManager = NavigationDomainManager(compassManager, locationManager, persistence)
  }

  //endregion

  //region Coordinates

  @Test
  fun `should return coordinates from persistence`() {
    // Given.
    Mockito.`when`(persistence.observeLatitude())
      .thenReturn(Flowable.just(TEST_LATITUDE))

    Mockito.`when`(persistence.observeLongitude())
      .thenReturn(Flowable.just(TEST_LONGITUDE))

    // When.
    val subscriber =
      navigationManager.observeCachedCoordinates().test()

    // Then.
    subscriber.assertResult(
      Coordinates(
        TEST_LATITUDE.toDouble(),
        TEST_LONGITUDE.toDouble()
      )
    )
  }

  //endregion

  //region Azimuth

  @Test
  fun `should return azimuth`() {
    // Given.
    Mockito.`when`(compassManager.observeEvents())
      .thenReturn(Observable.just(1.0f))

    // When.
    val subscriber =
      navigationManager.observeAzimuth().test()

    // Then.
    subscriber
      .assertSubscribed()
      .assertValues(1.0f)
  }

  //endregion


}
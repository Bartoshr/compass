package io.github.bartoshr.compass.domain.manager

import io.github.bartoshr.compass.domain.model.location.Coordinates
import io.github.bartoshr.compass.persistence.manager.LocationPersistenceManager
import io.github.bartoshr.compass.utility.compass.CompassManager
import io.github.bartoshr.compass.utility.location.LocationManager
import io.reactivex.Completable
import io.reactivex.rxkotlin.zipWith
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NavigationDomainManager @Inject constructor(
  private val compassManager: CompassManager,
  private val locationManager: LocationManager,
  private val persistence: LocationPersistenceManager
) {

  //region Azimuth

  fun observeAzimuth() =
    compassManager
      .observeEvents()

  //endregion

  //region Coordinates

  fun observeCachedCoordinates() =
    persistence.observeLatitude()
      .zipWith(persistence.observeLongitude())
      .map {
        Coordinates(
          it.first.toDouble(),
          it.second.toDouble()
        )
      }

  fun saveCoordinates(lat: String, lng: String): Completable =
    persistence.setLatitude(lat)
      .andThen(persistence.setLongitude(lng))

  //endregion

  //region Distance

  fun observeDistance() =
    locationManager.observeLocation()
      .map { currentLocation ->
        currentLocation.distanceTo(
          persistence.getCachedLocation()
        )
      }

  //endregion

  //region Bearing

  fun observeBearing() =
    locationManager.observeLocation()
      .map { currentLocation ->
        currentLocation.bearingTo(
          persistence.getCachedLocation()
        )
      }

  //endregion

}
package io.github.bartoshr.compass.domain.model.location

data class Coordinates(
  val latitude: Double = 0.0,
  val longitude: Double = 0.0
)

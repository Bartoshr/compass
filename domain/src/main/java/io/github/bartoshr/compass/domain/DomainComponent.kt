package io.github.bartoshr.compass.domain

import android.content.Context
import android.content.res.Resources
import com.facebook.stetho.Stetho
import dagger.BindsInstance
import dagger.Component
import io.github.bartoshr.compass.domain.manager.NavigationDomainManager
import javax.inject.Singleton

@Singleton
@Component(modules = [DomainModule::class])
interface DomainComponent {

  //region Component Builder

  @Component.Builder
  interface Builder {

    @BindsInstance
    fun seedInstance(context: Context): Builder

    @BindsInstance
    fun seedInstance(resources: Resources): Builder

    fun build(): DomainComponent
  }

  //endregion

  //region Share Managers

  fun navigationManager(): NavigationDomainManager

  //endregion

  //region Share Various

  fun stethoInitializer(): Stetho.Initializer

  //endregion
}

package io.github.bartoshr.compass.domain

import dagger.Module
import io.github.bartoshr.compass.domain.mapper.MapperModule
import io.github.bartoshr.compass.persistence.PersistenceModule
import io.github.bartoshr.compass.utility.UtilityModule

@Module(includes = [
  MapperModule::class,
  PersistenceModule::class,
  UtilityModule::class
])
class DomainModule

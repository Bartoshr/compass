package io.github.bartoshr.compass.persistence.manager

import android.location.Location
import io.github.bartoshr.compass.persistence.cache.location.LocationCache
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.annotations.SchedulerSupport
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@SchedulerSupport(value = SchedulerSupport.IO)
class LocationPersistenceManager @Inject constructor(
  private val locationCache: LocationCache
) {

  //region Const

  companion object {
    const val LOCATION_PROVIDER = "LocationPersistenceManager"
  }

  //endregion

  //region Location

  fun getCachedLocation() =
    Location(LOCATION_PROVIDER).apply {
      latitude = this@LocationPersistenceManager.getLatitude().toDouble()
      longitude = this@LocationPersistenceManager.getLongitude().toDouble()
    }

  //endregion

  //region Latitude

  fun observeLatitude(): Flowable<String> =
    locationCache.observeRxLatitude()

  fun getLatitude(): String =
    locationCache.getLatitude()

  fun setLatitude(value: String): Completable =
    locationCache.setRxLatitude(value)

  //endregion

  //region Longitude

  fun observeLongitude(): Flowable<String> =
    locationCache.observeRxLongitude()

  fun getLongitude(): String =
    locationCache.getLongitude()

  fun setLongitude(value: String): Completable =
    locationCache.setRxLongitude(value)

  //endregion
}

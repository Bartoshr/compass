package io.github.bartoshr.compass.persistence.cache.location

import co.windly.ktxprefs.annotation.DefaultString
import co.windly.ktxprefs.annotation.Name
import co.windly.ktxprefs.annotation.Prefs

@Prefs(fileName = "LocationCacheProvider")
data class LocationCacheProvider(

  //region Coordinates

  @Name(value = "latitude")
  @DefaultString(value = "0.0")
  internal var latitude: String,

  @Name(value = "longitude")
  @DefaultString(value = "0.0")
  internal var longitude: String

  //endregion
)

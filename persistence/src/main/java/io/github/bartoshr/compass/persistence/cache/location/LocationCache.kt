package io.github.bartoshr.compass.persistence.cache.location

import android.content.Context
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocationCache @Inject constructor(context: Context) :
  LocationCacheProviderPrefs(context.requireLocationCacheProvider())

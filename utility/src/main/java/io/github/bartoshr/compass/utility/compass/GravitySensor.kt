package io.github.bartoshr.compass.utility.compass

import javax.inject.Qualifier
import kotlin.annotation.AnnotationRetention.RUNTIME

@Qualifier
@Retention(RUNTIME)
annotation class GravitySensor(val value: String = "gravity-sensor")
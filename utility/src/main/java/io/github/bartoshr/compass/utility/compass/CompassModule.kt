package io.github.bartoshr.compass.utility.compass

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CompassModule {

  //region Sensor Manager

  @Provides
  @Singleton
  internal fun provideSensorManager(context: Context) =
    context.getSystemService(Context.SENSOR_SERVICE) as SensorManager

  //endregion

  //region Sensors

  @Provides
  @Singleton
  @GravitySensor
  internal fun provideGravitySensor(sensorManager: SensorManager) =
    sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)

  @Provides
  @Singleton
  @MagneticSensor
  internal fun provideMagneticSensor(sensorManager: SensorManager) =
    sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)

  //endregion

}
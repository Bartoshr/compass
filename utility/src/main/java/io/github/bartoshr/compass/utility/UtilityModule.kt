package io.github.bartoshr.compass.utility

import dagger.Module
import io.github.bartoshr.compass.utility.compass.CompassModule
import io.github.bartoshr.compass.utility.debug.StethoModule
import io.github.bartoshr.compass.utility.location.LocationModule

@Module(includes = [
  StethoModule::class,
  CompassModule::class,
  LocationModule::class
])
class UtilityModule

package io.github.bartoshr.compass.utility.type

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import io.github.bartoshr.compass.utility.type.DateUtil.Companion.DATE_FORMAT_TEMP_PHOTO

class DateUtil {

  companion object {

    /**
     * Date time format used to name files.
     */
    const val DATE_FORMAT_TEMP_PHOTO = "yyyy_MM_dd_HH_mm_ss"
  }
}

//region Time Zone

internal fun defaultTimeZone() =
  DateTimeZone.getDefault()

//endregion

//region Date Time

fun DateTime.formatForTempPhoto(): String =
  DateTimeFormat.forPattern(DATE_FORMAT_TEMP_PHOTO)
    .withZone(defaultTimeZone())
    .print(this)

//endregion
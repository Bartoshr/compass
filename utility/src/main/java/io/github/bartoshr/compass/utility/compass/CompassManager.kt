package io.github.bartoshr.compass.utility.compass

import android.hardware.Sensor
import android.hardware.Sensor.TYPE_ACCELEROMETER
import android.hardware.Sensor.TYPE_MAGNETIC_FIELD
import android.hardware.SensorEvent
import android.hardware.SensorEventListener2
import android.hardware.SensorManager
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CompassManager @Inject constructor(
  private val sensorManager: SensorManager,
  @MagneticSensor val magneticSensor: Sensor,
  @GravitySensor val gravitySensor: Sensor
) : SensorEventListener2 {

  //region Publish Subject

  private val subject: Subject<Float> = PublishSubject.create<Float>().toSerialized()

  //endregion

  //region Watching

  private fun startWatching() {
    // Listen gravity sensor events.
    sensorManager.registerListener(this, gravitySensor,
      SensorManager.SENSOR_DELAY_GAME)

    // Listen magnetic sensor events.
    sensorManager.registerListener(this, magneticSensor,
      SensorManager.SENSOR_DELAY_GAME)
  }

  private fun stopWatching() {
    sensorManager.unregisterListener(this)
  }

  //endregion

  //region Data

  private val mGravity = FloatArray(3)
  private val mGeomagnetic = FloatArray(3)
  private val R = FloatArray(9)
  private val I = FloatArray(9)
  private val alpha = 0.03f

  private var azimuth = 0f

  //endregion

  //region Interface

  fun observeEvents(): Observable<Float> =
    subject
      .doOnSubscribe { startWatching() }
      .doOnDispose { stopWatching() }
      .share()

  //endregion

  //region Callbacks

  override fun onSensorChanged(event: SensorEvent?) {

    event?.sensor?.let { sensor ->

      synchronized(this) {

        when(sensor.type) {
          TYPE_ACCELEROMETER -> smoothing(mGravity, event, alpha)
          TYPE_MAGNETIC_FIELD -> smoothing(mGeomagnetic, event, alpha)
        }

        val success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic)

        if (success) {

          val orientation = FloatArray(3)
          SensorManager.getOrientation(R, orientation)

          azimuth = Math.toDegrees(orientation[0].toDouble()).toFloat() // orientation
          azimuth = (azimuth + 360) % 360
          subject.onNext(azimuth)
        }
      }

    }

  }

  override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    //no-op
  }

  override fun onFlushCompleted(sensor: Sensor?) {
    //no-op
  }

  //endregion

  //region Smoothing

  private fun smoothing(array: FloatArray, event : SensorEvent, alpha : Float) {
    array.forEachIndexed { i, _ ->
      array[i] = (1 - alpha) * array[i] + alpha * event.values[i]
    }
  }

  //endregion

}
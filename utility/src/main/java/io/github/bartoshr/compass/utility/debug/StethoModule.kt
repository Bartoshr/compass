package io.github.bartoshr.compass.utility.debug

import android.content.Context
import com.facebook.stetho.DumperPluginsProvider
import com.facebook.stetho.InspectorModulesProvider
import com.facebook.stetho.Stetho
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StethoModule {

  //region Initializer

  @Provides
  @Singleton
  internal fun provideInitializer(
    inspector: InspectorModulesProvider,
    dumper: DumperPluginsProvider,
    builder: Stetho.InitializerBuilder
  ) =
    builder
      .enableWebKitInspector(inspector)
      .enableDumpapp(dumper)
      .build()

  @Provides
  @Singleton
  internal fun provideInitializerBuilder(context: Context) =
    Stetho.newInitializerBuilder(context)

  //endregion

  //region Dumper Plugins Provider

  @Provides
  @Singleton
  internal fun provideDumperPluginsProvider(context: Context) =
    DumperPluginsProvider { Stetho.DefaultDumperPluginsBuilder(context).finish() }

  //endregion

  //region Inspector Modules Provider

  @Provides
  @Singleton
  internal fun provideInspectorModulesProvider(context: Context) =
    Stetho.defaultInspectorModulesProvider(context)

  //endregion
}

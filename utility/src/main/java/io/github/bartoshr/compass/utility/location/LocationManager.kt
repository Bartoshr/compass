package io.github.bartoshr.compass.utility.location

import android.location.Location
import android.os.Looper
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import io.github.bartoshr.compass.utility.log.ILogger
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class LocationManager @Inject constructor(
  private val fusedLocationClient: FusedLocationProviderClient,
  private val locationRequest: LocationRequest
) {

  //region Subject

  private val locationSubject = PublishSubject.create<Location>()

  //endregion

  //region Watching

  private fun startLocationUpdates() {
    try {
      val task = fusedLocationClient.lastLocation
      task.addOnSuccessListener(::setLocation)
      fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback,
        Looper.getMainLooper())
    } catch (error: SecurityException) {
      ILogger.e("Exception after request location updates $error")
    }
  }

  private fun stopLocationUpdates() {
    fusedLocationClient.removeLocationUpdates(locationCallback)
  }

  //endregion

  //region Callback

  private val locationCallback: LocationCallback = object : LocationCallback() {
    override fun onLocationResult(locationResult: LocationResult?) {
      locationResult?.locations?.forEach(::setLocation)
    }
  }

  private fun setLocation(location: Location) {
    // Log new location.
    ILogger.d("New location posted ${location.latitude}, ${location.longitude}")

    locationSubject.onNext(location)
  }

  //endregion

  //region Interface

  fun observeLocation(): Flowable<Location> =
    locationSubject.toFlowable(BackpressureStrategy.LATEST)
      .doOnSubscribe { startLocationUpdates() }
      .doOnCancel { stopLocationUpdates() }
      .share()


  //endregion

}
package io.github.bartoshr.compass.utility.location

import javax.inject.Qualifier
import kotlin.annotation.AnnotationRetention.RUNTIME

@Qualifier
@Retention(RUNTIME)
annotation class LocationRequestFastestInterval(val value: String = "request-fastest-interval")
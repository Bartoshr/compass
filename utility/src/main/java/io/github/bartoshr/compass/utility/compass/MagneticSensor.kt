package io.github.bartoshr.compass.utility.compass

import javax.inject.Qualifier
import kotlin.annotation.AnnotationRetention.RUNTIME

@Qualifier
@Retention(RUNTIME)
annotation class MagneticSensor(val value: String = "magnetic-sensor")
package io.github.bartoshr.compass.utility.location

import android.content.Context
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import io.github.bartoshr.compass.utility.BuildConfig.LOCATION_REQUEST_FASTEST_INTERVAL
import io.github.bartoshr.compass.utility.BuildConfig.LOCATION_REQUEST_INTERVAL
import javax.inject.Singleton

@Module
class LocationModule {

  //region Fused Location Provider Client

  @Provides
  @Singleton
  internal fun provideFusedLocationProviderClient(context: Context) =
    LocationServices.getFusedLocationProviderClient(context)

  //endregion

  //region Intervals

  @Provides
  @Singleton
  @LocationRequestInterval
  internal fun provideLocationRequestInterval() = LOCATION_REQUEST_INTERVAL


  @Provides
  @Singleton
  @LocationRequestFastestInterval
  internal fun provideLocationRequestFastestInterval() = LOCATION_REQUEST_FASTEST_INTERVAL

  //endregion

  //region Location Request

  @Provides
  @Singleton
  internal fun provideLocationRequest(
    @LocationRequestInterval locationRequestInterval: Long,
    @LocationRequestFastestInterval locationRequestFastestInterval: Long
  ) = LocationRequest.create().apply {
    interval = locationRequestInterval
    fastestInterval = locationRequestFastestInterval
    priority = LocationRequest.PRIORITY_HIGH_ACCURACY
  }

  //endregion

}
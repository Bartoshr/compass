package io.github.bartoshr.compass.utility.location

import javax.inject.Qualifier
import kotlin.annotation.AnnotationRetention.RUNTIME

@Qualifier
@Retention(RUNTIME)
annotation class LocationRequestInterval(val value: String = "request-interval")
package io.github.bartoshr.compass.application

import co.windly.limbo.dagger.scope.ApplicationScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import io.github.bartoshr.compass.domain.DomainComponent
import io.github.bartoshr.compass.presentation.PresentationModule

@ApplicationScope
@Component(
  dependencies = [DomainComponent::class],
  modules = [
    AndroidInjectionModule::class,
    ApplicationModule::class,
    PresentationModule::class
  ])
interface ApplicationComponent : AndroidInjector<Compass> {

  //region Component Builder

  @Component.Builder
  interface Builder {

    @BindsInstance
    abstract fun seedInstance(application: Compass): Builder

    abstract fun seedInstance(component: DomainComponent): Builder

    abstract fun build(): ApplicationComponent
  }

  //endregion
}

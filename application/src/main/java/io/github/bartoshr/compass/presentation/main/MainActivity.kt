package io.github.bartoshr.compass.presentation.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import io.github.bartoshr.compass.R
import io.github.bartoshr.compass.databinding.ActivityMainBinding
import io.github.bartoshr.compass.presentation.base.activity.BaseActivity

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(), MainTrait {

  //region View Model

  override val viewModel: MainViewModel
    by viewModels { factory }

  //endregion

  //region Ui

  override val layoutResId: Int
    get() = R.layout.activity_main

  //endregion

  //region Lifecycle

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    // Search for navigation host fragment.
    val host = supportFragmentManager
      .findFragmentById(R.id.navFragment) as NavHostFragment

    // Find nav controller.
    navigationController = host.navController
  }

  //endregion

  //region Navigation Controller

  private lateinit var navigationController: NavController

  //endregion
}

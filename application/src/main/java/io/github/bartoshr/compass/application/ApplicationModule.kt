package io.github.bartoshr.compass.application

import android.content.ContentResolver
import android.content.Context
import android.content.res.Resources
import co.windly.limbo.dagger.scope.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {

  //region Context

  @Provides
  @ApplicationScope
  internal fun provideApplicationContext(application: Compass): Context =
    application

  @Provides
  @ApplicationScope
  internal fun provideApplicationResources(application: Compass): Resources =
    application.resources

  //endregion

  //region Content Provider

  @Provides
  @ApplicationScope
  internal fun provideContentResolver(context: Context): ContentResolver =
    context.contentResolver

  //endregion

}

package io.github.bartoshr.compass.presentation.main.home

import co.windly.limbo.mvvm.trait.FragmentTrait
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import io.github.bartoshr.compass.R
import io.github.bartoshr.compass.presentation.base.trait.ConfirmExitTrait
import io.github.bartoshr.compass.presentation.base.trait.SnackbarTrait

//region Home

interface HomeTrait :
  ConfirmExitTrait, DestinationDialogTrait, HomeErrorTrait

//endregion

//region Home - Error

interface HomeErrorTrait : SnackbarTrait {

  fun showInvalidCoordinatesFormatInfo() =
    showLongSnackbar(R.string.home_error_invalid_coordinates_format)

}
//endregion

//region Home - Dialog

interface DestinationDialogTrait : FragmentTrait {

  fun showDestinationDialog(
    coordinatesListener: (String) -> Unit
  ): Unit = with(fragmentTrait) {

    MaterialDialog(requireContext())
      .apply {
        title(res = R.string.home_label_destination_tile)
        message(res = R.string.home_label_destination_ask)
        input(hintRes = R.string.home_hint_coordinates) { _, latitude ->
          // Text submitted with the action button
          coordinatesListener(latitude.toString())
        }
        negativeButton(res = R.string.common_cancel)
      }
      .show()
  }
}

//endregion
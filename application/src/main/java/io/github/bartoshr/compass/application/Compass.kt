package io.github.bartoshr.compass.application

import android.app.Application
import com.facebook.stetho.Stetho
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.github.bartoshr.compass.domain.DaggerDomainComponent
import io.github.bartoshr.compass.utility.BuildConfig.DEBUG
import io.github.bartoshr.compass.utility.BuildConfig.ENABLE_DEBUG_BRIDGE
import io.github.bartoshr.compass.utility.log.ILogger
import javax.inject.Inject

class Compass : Application(), HasAndroidInjector {

  //region Activity Injector

  @Inject
  lateinit var activityInjector: DispatchingAndroidInjector<Any>

  override fun androidInjector(): AndroidInjector<Any> =
    activityInjector

  //endregion

  //region Lifecycle

  override fun onCreate() {
    super.onCreate()

    // Initialize dependency injection.
    initializeDependencyInjection()

    // Initialize debug bridge.
    initializeDebugBridge()

    // Initialize logger.
    initializeLogger()
  }

  //endregion

  //region Dependency Injection

  private fun initializeDependencyInjection() {

    // Initialize domain component.
    val domainComponent = DaggerDomainComponent.builder()
      .seedInstance(this)
      .seedInstance(resources)
      .build()

    // Initialize application component.
    DaggerApplicationComponent.builder()
      .seedInstance(this)
      .seedInstance(domainComponent)
      .build()

      // Inject dependencies.
      .also { it.inject(this) }
  }

  //endregion

  //region Debug Bridge

  @Inject
  lateinit var stethoInitializer: Stetho.Initializer

  private fun initializeDebugBridge() {

    // Conditionally initialize debug bridge.
    if (ENABLE_DEBUG_BRIDGE) {
      Stetho
        .initialize(stethoInitializer)
    }
  }

  //endregion

  //region Logger

  private fun initializeLogger() {

    // Conditionally enable logger.
    ILogger
      .init(DEBUG)
  }

  //endregion

}

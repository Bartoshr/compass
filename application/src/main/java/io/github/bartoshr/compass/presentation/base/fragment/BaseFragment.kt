package io.github.bartoshr.compass.presentation.base.fragment

import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import co.windly.limbo.mvvm.fragment.DaggerMvvmFragment
import co.windly.limbo.mvvm.viewmodel.LimboViewModel
import io.github.bartoshr.compass.presentation.base.trait.SnackbarTrait

abstract class BaseFragment<Binding : ViewDataBinding, VM : LimboViewModel> :
  DaggerMvvmFragment<Binding, VM>(), SnackbarTrait {

  //region Traits

  override var snackbarTrait: View? = null

  //endregion

  //region Lifecycle

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    // Search for snackbar trait view.
    snackbarTrait =
      view.findViewById(snackbarTraitViewId)
  }

  //endregion

}

package io.github.bartoshr.compass.presentation.base.trait

import androidx.activity.OnBackPressedCallback
import co.windly.limbo.mvvm.trait.FragmentNavigationTrait
import com.afollestad.materialdialogs.MaterialDialog
import io.github.bartoshr.compass.R

interface ConfirmExitTrait : FragmentNavigationTrait {

  //region Activity

  private fun finishAffinity() {
    navigationTrait
      .requireActivity()
      .finishAffinity()
  }

  //endregion

  //region Callback

  val confirmExitTrait: ConfirmExitCallback

  fun confirmExitTemplate(onConfirmClicked: () -> Unit = {}): ConfirmExitCallback =
    ConfirmExitCallback(this, onConfirmClicked)

  fun registerConfirmExitCallback() {

    // Register callback in traits activity.
    navigationTrait
      .requireActivity()
      .onBackPressedDispatcher
      .addCallback(confirmExitTrait)
  }

  fun unregisterConfirmExitCallback() {
    confirmExitTrait.remove()
  }

  class ConfirmExitCallback(
    private val trait: ConfirmExitTrait,
    private val onConfirmClicked: () -> Unit
  ) : OnBackPressedCallback(true) {

    override fun handleOnBackPressed() {

      // Show exit confirmation dialog.
      trait.showConfirmCloseDialog(onConfirmClicked)
    }
  }

  //endregion

  //region Dialog

  private fun showConfirmCloseDialog(onConfirmClicked: () -> Unit) {

    // Show material dialog to ask user for confirmation.
    MaterialDialog(navigationTrait.requireContext()).show {

      // Configure title.
      title(R.string.common_confirmation)

      // Configure message.
      message(R.string.common_quit)

      // Configure negative button.
      negativeButton(R.string.common_cancel)

      // Configure positive button.
      positiveButton(R.string.common_ok) {

        // Do additional action on confirm click.
        onConfirmClicked.invoke()

        // Finish activity.
        finishAffinity()
      }
    }
  }

  //endregion
}

package io.github.bartoshr.compass.presentation

import androidx.lifecycle.ViewModelProvider
import co.windly.limbo.dagger.lifecycle.ViewModelFactory
import dagger.Binds
import dagger.Module
import io.github.bartoshr.compass.presentation.main.MainModule

@Module(includes = [
  MainModule::class
])
abstract class PresentationModule {

  //region View Model Factory

  @Binds
  abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

  //endregion
}

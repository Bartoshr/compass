package io.github.bartoshr.compass.presentation.main.home

import android.content.Context
import dagger.Reusable
import io.github.bartoshr.compass.R
import io.github.bartoshr.compass.domain.model.location.Coordinates
import javax.inject.Inject

@Reusable
class HomeResources @Inject constructor(private val context: Context) {

  //region Distance

  fun distanceLabel(distance: Float): String =
    context.getString(R.string.home_label_coordinates, distance)

  //endregion

  //region Destination

  fun destinationLabel(coordinates: Coordinates): String =
    context.getString(
      R.string.home_label_destination_inform,
      coordinates.latitude,
      coordinates.longitude
    )

  //endregion

  //region Permissions

  fun permissionsLabel(): String =
    context.getString(
      R.string.home_label_permission_inform
    )

  //endregion
}
package io.github.bartoshr.compass.presentation.binding

import android.view.animation.Animation.RELATIVE_TO_SELF
import android.view.animation.RotateAnimation
import android.widget.ImageView
import androidx.databinding.BindingAdapter


//region Rotation

data class Rotation(val from: Float, val to: Float)

@BindingAdapter(
  requireAll = true,
  value = ["rotation"]
)
fun setRotation(view: ImageView, rotation: Rotation) {

  // Set cursor position.
  val animation = RotateAnimation(
    -rotation.from,
    -rotation.to,
    RELATIVE_TO_SELF,
    0.5f,
    RELATIVE_TO_SELF,
    0.5f
  )

  // Prepare
  animation.duration = 500
  animation.repeatCount = 0
  animation.fillAfter = true

  view.startAnimation(animation)
}

//endregion
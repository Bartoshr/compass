package io.github.bartoshr.compass.presentation.main

import androidx.lifecycle.ViewModel
import co.windly.limbo.dagger.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import io.github.bartoshr.compass.presentation.main.home.HomeModule

@Module(includes = [
  HomeModule::class
])
abstract class MainModule {

  //region Binding

  @Binds
  @IntoMap
  @ViewModelKey(MainViewModel::class)
  abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

  //endregion

  //region Contribution

  @ContributesAndroidInjector
  abstract fun contributeMainActivity(): MainActivity

  //endregion
}

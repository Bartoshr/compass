package io.github.bartoshr.compass.presentation.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.windly.limbo.mvvm.lifecycle.SingleLiveEvent
import co.windly.limbo.mvvm.viewmodel.LimboViewModel
import co.windly.limbo.utility.reactive.addTo
import co.windly.limbo.utility.reactive.observeOnUi
import io.github.bartoshr.compass.domain.manager.NavigationDomainManager
import io.github.bartoshr.compass.domain.model.location.Coordinates
import io.github.bartoshr.compass.presentation.binding.Rotation
import io.github.bartoshr.compass.utility.log.ILogger
import javax.inject.Inject

class HomeViewModel @Inject constructor(
  private val navigationManager: NavigationDomainManager,
  val resources: HomeResources
) : LimboViewModel() {

  //region Azimuth

  private val _azimuth: MutableLiveData<Rotation> =
    MutableLiveData(
      Rotation(0f, 0f)
    )

  val azimuth: LiveData<Rotation> =
    _azimuth

  fun startObserveAzimuth() {
    navigationManager
      .observeAzimuth()
      .observeOnUi()
      .subscribe(
        this::handleObserveAzimuthSuccess,
        this::handleObserveAzimuthError
      ).addTo(disposables)
  }

  private fun handleObserveAzimuthSuccess(azimuth: Float) {

    // Fetch previous value of azimuth.
    val previousAzimuth = _azimuth.value?.to ?: 0f

    ILogger.d("Azimuth change from $previousAzimuth to $azimuth")

    // Post new value.
    _azimuth.postValue(
      Rotation(previousAzimuth, azimuth)
    )

    // Update bearing as well.
    updateBearing(azimuth)
  }

  private fun handleObserveAzimuthError(throwable: Throwable) {
    // Log an error.
    ILogger.e("An error occurred while observing azimuth")
    ILogger.e(throwable)
  }

  //endregion

  //region Bearing

  private val _bearing: MutableLiveData<Rotation> =
    MutableLiveData(
      Rotation(0f, 0f)
    )

  val bearing: LiveData<Rotation> =
    _bearing

  var lastBearing: Float = 0.0f

  fun startObserveBearing() {
    navigationManager
      .observeBearing()
      .observeOnUi()
      .subscribe(
        this::handleObserveBearingSuccess,
        this::handleObserveBearingError
      ).addTo(disposables)
  }

  private fun handleObserveBearingSuccess(bearing: Float) {

    ILogger.d("Bearing change to $bearing")

    lastBearing = bearing
  }

  private fun handleObserveBearingError(throwable: Throwable) {
    // Log an error.
    ILogger.e("An error occurred while trying to observe bearing")
    ILogger.e(throwable)
  }

  private fun updateBearing(azimuth: Float) {
    // Fetch previous values of bearing and azimuth.
    val prevBearing = _bearing.value?.to ?: 0f

    // Calculate new angle.
    val newBearing = azimuth + lastBearing

    // Post new value.
    _bearing.postValue(
      Rotation(prevBearing, newBearing)
    )
  }

  //endregion

  //region Destination Error

  private val _invalidDestinationError: SingleLiveEvent<Any> =
    SingleLiveEvent()

  val invalidDestinationError: LiveData<Any> =
    _invalidDestinationError

  //endregion

  //region Destination Change

  private val _destinationChangeRequested: SingleLiveEvent<Any> =
    SingleLiveEvent()

  val destinationChangeRequested: LiveData<Any> =
    _destinationChangeRequested

  fun onChangeDestinationClicked() {
    _destinationChangeRequested.post()
  }

  companion object Pattern {
    val coordinatesPattern = "(\\d+\\.\\d+),\\s*(\\d+\\.\\d+)".toRegex()
  }

  fun onDestinationChanged(coordinatesData: String) {

    // Retrieve possible name results.
    val possibleCoordinates = coordinatesPattern.find(coordinatesData.trim())

    // Retrieve possible name.
    val latitudeCoordinate = possibleCoordinates?.groupValues?.get(1)
    val longitudeCoordinate = possibleCoordinates?.groupValues?.get(2)

    if (latitudeCoordinate.isNullOrEmpty()) {
      _invalidDestinationError.post()
      return
    }

    if (longitudeCoordinate.isNullOrEmpty()) {
      _invalidDestinationError.post()
      return
    }

    navigationManager
      .saveCoordinates(latitudeCoordinate, longitudeCoordinate)
      .observeOnUi()
      .subscribe(
        this::handleSaveCoordinatesSuccess,
        this::handleSaveCoordinatesError
      ).addTo(disposables)
  }


  private fun handleSaveCoordinatesSuccess() {
    ILogger.d("Successfully saved coordinates")
  }

  private fun handleSaveCoordinatesError(throwable: Throwable) {
    // Log an error.
    ILogger.e("An error occurred while trying to save coordinates")
    ILogger.e(throwable)
  }

  //endregion

  //region Destination

  private val _destinationLabel: MutableLiveData<String> =
    MutableLiveData()

  val destinationLabel: LiveData<String> =
    _destinationLabel

  fun startObserveDestination() {
    navigationManager
      .observeCachedCoordinates()
      .observeOnUi()
      .subscribe(
        this::handleObserveCachedCoordinatesSuccess,
        this::handleObserveCachedCoordinatesError
      ).addTo(disposables)
  }

  private fun handleObserveCachedCoordinatesSuccess(coordinates: Coordinates) {

    ILogger.d("Cached coordinates changed to $coordinates")

    // Post value.
    _destinationLabel.postValue(
      resources.destinationLabel(coordinates)
    )
  }

  private fun handleObserveCachedCoordinatesError(throwable: Throwable) {
    // Log an error.
    ILogger.e("An error occurred while observe cached coordinates")
    ILogger.e(throwable)
  }

  //endregion

  //region Permission

  private val _permissionRequest: SingleLiveEvent<Any> =
    SingleLiveEvent()

  val permissionRequest: LiveData<Any> =
    _permissionRequest

  fun onLocationPermissionDenied() {
    _distanceLabel.postValue(
      resources.permissionsLabel()
    )
  }

  fun requestLocationPermissions() {
    _permissionRequest.post()
  }

  //endregion

  //region Distance

  private val _distanceLabel: MutableLiveData<String> =
    MutableLiveData()

  val distanceLabel: LiveData<String> =
    _distanceLabel

  fun startObserveDistance() {
    navigationManager
      .observeDistance()
      .observeOnUi()
      .subscribe(
        this::handleObserveDistanceSuccess,
        this::handleObserveDistanceError
      ).addTo(disposables)
  }

  private fun handleObserveDistanceSuccess(distance: Float) {

    ILogger.d("Distance change to $distance")

    // Post value.
    _distanceLabel.postValue(
      resources.distanceLabel(distance)
    )
  }

  private fun handleObserveDistanceError(throwable: Throwable) {
    // Log an error.
    ILogger.e("An error occurred while observe distance")
    ILogger.e(throwable)
  }

  //endregion

}

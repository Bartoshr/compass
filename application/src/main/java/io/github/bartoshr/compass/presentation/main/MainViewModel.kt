package io.github.bartoshr.compass.presentation.main

import co.windly.limbo.mvvm.viewmodel.LimboViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor() : LimboViewModel()

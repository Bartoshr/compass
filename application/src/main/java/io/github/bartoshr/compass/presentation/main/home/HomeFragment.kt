package io.github.bartoshr.compass.presentation.main.home

import android.Manifest
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import io.github.bartoshr.compass.R
import io.github.bartoshr.compass.databinding.FragmentHomeBinding
import io.github.bartoshr.compass.presentation.base.fragment.BaseFragment
import io.github.bartoshr.compass.presentation.base.trait.ConfirmExitTrait.ConfirmExitCallback
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(), HomeTrait {

  //region Ui

  override val layoutRes: Int
    get() = R.layout.fragment_home

  //endregion

  //region View Model

  override val viewModel: HomeViewModel
    by viewModels { factory }

  //endregion

  //region Traits

  override val confirmExitTrait: ConfirmExitCallback
    by lazy { confirmExitTemplate() }

  //endregion

  //region Binding

  override fun bindView(binding: FragmentHomeBinding) {
    binding
      .also { it.disposables = disposables }
      .also { it.viewModel = viewModel }
  }

  //endregion

  //region Lifecycle

  override fun onRequestPermissionsResult(
    requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    // Delegate to permission dispatcher.
    onRequestPermissionsResult(requestCode, grantResults)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    observeAzimuth()
    observeDestination()

    // Start coordinates observation.
    observeLocationRelatedValuesWithPermissionCheck()

    viewModel
      .destinationChangeRequested
      .observe(viewLifecycleOwner) { handleDestinationChangeRequested() }

    viewModel
      .invalidDestinationError
      .observe(viewLifecycleOwner) { showInvalidCoordinatesFormatInfo() }

    viewModel
      .permissionRequest
      .observe(viewLifecycleOwner) { observeLocationRelatedValuesWithPermissionCheck() }
  }

  override fun onPause() {
    super.onPause()

    // Unregister back pressed callback.
    unregisterConfirmExitCallback()
  }

  override fun onResume() {
    super.onResume()

    // Register back pressed callback.
    registerConfirmExitCallback()
  }

  //endregion

  //region Azimuth - Observe

  private fun observeAzimuth() {
    viewModel.startObserveAzimuth()
  }

  //endregion

  //region Destination

  private fun observeDestination() {
    viewModel.startObserveDestination()
  }

  private fun handleDestinationChangeRequested() {
    showDestinationDialog(
      viewModel::onDestinationChanged
    )
  }

  //endregion

  //region Location Permission Denied

  @OnPermissionDenied(
    value = [
      Manifest.permission.ACCESS_FINE_LOCATION,
      Manifest.permission.ACCESS_COARSE_LOCATION
    ]
  )
  fun locationPermissionsDenied(){
    viewModel.onLocationPermissionDenied()
  }

  //endregion

  //region Location Relative Values - Observe

  @NeedsPermission(value = [
    Manifest.permission.ACCESS_FINE_LOCATION,
    Manifest.permission.ACCESS_COARSE_LOCATION
  ])
  fun observeLocationRelatedValues() {
    viewModel.startObserveDistance()
    viewModel.startObserveBearing()
  }

  //endregion

}
